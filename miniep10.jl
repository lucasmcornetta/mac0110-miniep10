function sum_submatrix(matrix,i,j,n)
    sum = 0
    for x in i:(i+n-1)
	for y in j:(j+n-1)
	    sum += matrix[x,y]
	end
    end
    return sum
end

# print submatrix
function submatrix(matrix,i,j,n)
    submatrix = zeros(Int,n,n)
    for x in 1:n
	for y in 1:n
	    submatrix[x,y] = matrix[i+x-1,j+y-1] 
	end
    end
    return submatrix
end

function max_sum_submatrix(matrix)
    dim = size(matrix)
    if dim[1] != dim[2]
	println("Insira uma matriz quadrada")
	return -1
    end
    N = dim[1]
    soma_max = matrix[1,1]
    i_max = 1
    j_max = 1
    n_max = 1
    for i in 1:N
	for j in 1:N
	q = min(N-i,N-j)
	    for k in 1:q+1
	        sum = sum_submatrix(matrix,i,j,k)
	        if sum > soma_max
	            soma_max = sum
		    i_max = i
		    j_max = j
		    n_max = k
	        end
	    end
	    j += 1
	end
	i += 1
    end
    collection = []
    for i in 1:N
	for j in 1:N
            q = min(N-i,N-j)
            for k in 1:q+1
                sum = sum_submatrix(matrix,i,j,k)
                if sum == soma_max
                    push!(collection,submatrix(matrix,i,j,k))
	        end
            end
            j += 1
        end
    	i += 1
    end    
    return collection
end 
